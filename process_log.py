# -*- coding: utf-8 -*-
from datetime import datetime
from json import dumps, loads
from typing import NamedTuple, TypeAlias

LogDict: TypeAlias = dict[str, int | str | dict[str, str | list[str] | dict[str, str]]]


class LogRecord(NamedTuple):
    level: str
    time: int
    name: str
    msg: str

    @property
    def timestamp(self):
        return datetime.fromtimestamp(self.time // 1000)

    def __str__(self):
        return f"{self.level.upper()} :: {self.timestamp} :: {self.name}\n{self.msg}"

    def __repr__(self):
        return f"<{self.__class__.__name__}>{self._asdict().items()}"


def get_log_files(input_file: str, output_file: str):
    logs: list[str] = []

    with open(input_file, "r") as f:
        _content: list[str] = f.readlines()

    for _line in _content:
        log_dict: LogDict = loads(_line)

        level: str = log_dict.get("level")
        time: int = log_dict.get("time")
        name: str = log_dict.get("name")
        msg: str = log_dict.get("msg")

        log_record: LogRecord = LogRecord(level, time, name, msg)
        logs.append(str(log_record))

    _log_line: str = "\n".join([dumps(log, ensure_ascii=False, indent=2) for log in logs])

    with open(output_file, "w") as f:
        f.write(_log_line)


if __name__ == '__main__':
    get_log_files("antora.json", "antora.log")
